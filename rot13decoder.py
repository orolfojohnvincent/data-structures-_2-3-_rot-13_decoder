#list initialization
msgs = []
trans = []

#where user enter message
def decoder():
    intro = int(input("Enter no. of messsage: "))

    for i in range(0,intro):
        msg = str(input("Input text: "))
        msgs.append(msg)
        trans.append(rot13(msg))
        print("Decipher: " + rot13(msg))

    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" + "Decoding successful!\n" + "What's next?")
    menu()
    
#decoder function
def rot13(text):
   apbt = "abcdefghijklmnopqrstuvwxyz"
   result = ""
   for char in text:
       result += apbt[(apbt.find(char)+13)%26]
   return result

#view previous decipher
def history():
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")

    for x in range(len(msgs)):
        print ("ROT-13\t\t" + "Decipher\n" + msgs[x] + "\t\t" + trans[x])

    print("ALL RECORDS DISPLAYED")
    menu()

#start-up menu
def menu():
    print("ROT-13 ENCRYPTOR")
    print("~~~~~~~~~~~~~~~~~~~~~~~~")
    print("a. Decode, b. Decipher History, c. Exit")
    switch = input("Enter: ")
    if switch=='a':
        decoder()
    elif switch=='b':
        history()
    elif switch=='c':
        exit()
    else:
        print("Invalid input")
        menu()

#menu function call
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n")
menu()
